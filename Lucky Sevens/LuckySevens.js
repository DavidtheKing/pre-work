function demoVisibility() {
   document.getElementById("results").style.display = "none";
}

function playGame() {

	var startingBet = document.getElementById("Bet").value;
	var myBet = startingBet;
	var numberBets =[];
	var counter = 0;
	var highestMoney = myBet;
	var counterHighestMoney = 0; 


	while (myBet > 0){

    	counter += 1;
		
	var diceOne = Math.floor(Math.random() * 6) + 1;
	var diceTwo = Math.floor(Math.random() * 6) + 1;
	var diceRoll = diceOne + diceTwo;
	
		if (diceRoll != 7)
		{
			myBet -= 1;
		}
		else {myBet += 4;
		} 
		
		if (myBet > highestMoney) {
			highestMoney = myBet; 
			counterHighestMoney=counter;
		}

	}

	function showResults() {	
    document.getElementById("results").style.display = "inline";
    document.getElementById("playGame").innerHTML = "Play Again";
    document.getElementById("theBet").innerHTML = "$" + startingBet +".00";
    document.getElementById("amountRolls").innerHTML = counter;
    document.getElementById("highestHeld").innerHTML = "$" +  highestMoney + ".00";
    document.getElementById("rollsAtHighest").innerHTML = counterHighestMoney;
    };
    showResults();
}